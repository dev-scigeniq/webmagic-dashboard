<?php

Route::group([
    'prefix'     => 'dashboard/api',
    'as'         => 'dashboard.api.',
    'namespace'  => 'Scigeniq\Dashboard\Api\Http',
    'middleware' => config('scigeniq.dashboard.dashboard.api_middleware'),
], function () {
    /**
     * Images API
     */
    Route::post('image/{field_name}', [
       'as' => 'image.upload',
       'uses' => 'ImagesController@upload'
    ]);

    Route::delete('image', [
        'as' => 'image.delete',
        'uses' => 'ImagesController@delete'
    ]);

});

Route::group([
    'prefix'     => 'dashboard/lang',
    'as'         => 'dashboard.lang.',
    'namespace'  => 'Scigeniq\Dashboard\Api\Http',
    'middleware' => ['web'],
], function () {

    Route::post('set-locale', [
        'as' => 'set-locale',
        'uses' => 'SettingsController@setLocale'
    ]);
});
