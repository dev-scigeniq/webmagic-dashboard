<?php


namespace Scigeniq\Dashboard\Docs\Http;


use Scigeniq\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Scigeniq\Dashboard\Dashboard;

class JSScriptsPresentationController
{
    /**
     * Tooltips functionality presentation
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function sortable(Dashboard $dashboard): Dashboard
    {
        $content = view()->file(__DIR__ . '/../../../docs/js-scripts/sortable.md');

        $dashboard->page()
            ->setPageTitle('Sortable JS script')
            ->addElement()
            ->box()
            ->makeSimple()
            ->content($content);

        return $dashboard;
    }

    /**
     * Tooltips functionality presentation
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function controlledCheckboxes(Dashboard $dashboard): Dashboard
    {
        $content = view()->file(__DIR__ . '/../../../docs/js-scripts/controlled-checkboxes.md');

        $dashboard->page()
            ->setPageTitle('Controlled Checkboxes JS script')
            ->addElement()
            ->box()
            ->makeSimple()
            ->content($content);

        return $dashboard;
    }

    /**
     * Bootstrap dateRangePicker for input functionality presentation
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function dateRangePicker(Dashboard $dashboard): Dashboard
    {
        $content = view()->file(__DIR__ . '/../../../docs/js-scripts/date-range-picker.md');

        $dashboard->page()
            ->setPageTitle('Bootstrap DateRangePicker JS script')
            ->addElement()
            ->box()
            ->makeSimple()
            ->content($content);

        return $dashboard;
    }

    /**
     * Notifications functionality presentation
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function notifications(Dashboard $dashboard): Dashboard
    {
        $content = view()->file(__DIR__ . '/../../../docs/js-scripts/notifications.md');

        $dashboard->page()
            ->setPageTitle('Notifications JS script')
            ->addElement()
            ->box()
            ->makeSimple()
            ->content($content);

        return $dashboard;
    }

    /**
     * Sending Ajax functionality presentation
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function sendAjax(Dashboard $dashboard): Dashboard
    {
        $content = view()->file(__DIR__ . '/../../../docs/js-scripts/send-ajax.md');

        $dashboard->page()
            ->setPageTitle('Sending Ajax JS script')
            ->addElement()
            ->box()
            ->makeSimple()
            ->content($content);

        return $dashboard;
    }

    /**
     * Input mask functionality presentation
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function inputMask(Dashboard $dashboard): Dashboard
    {
        $content = view()->file(__DIR__ . '/../../../docs/js-scripts/input-mask.md');

        $dashboard->page()
            ->setPageTitle('Input mask JS script')
            ->addElement()
            ->box()
            ->makeSimple()
            ->content($content);

        return $dashboard;
    }

    /**
     * File uploader functionality presentation
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function fileUploader(Dashboard $dashboard): Dashboard
    {
        $content = view()->file(__DIR__ . '/../../../docs/js-scripts/file-uploader.md');

        $dashboard->page()
            ->setPageTitle('File uploader JS script')
            ->addElement()
            ->box()
            ->makeSimple()
            ->content($content);

        return $dashboard;
    }

    /**
     * Added dynamic multifields complex functionality presentation
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function dynamicMultiFieldsComplex(Dashboard $dashboard): Dashboard
    {
        $content = view()->file(__DIR__ . '/../../../docs/js-scripts/dynamic-multifields-complex.md');

        $dashboard->page()
            ->setPageTitle('Dynamically adding multiFields complex JS script')
            ->addElement()
            ->box()
            ->makeSimple()
            ->content($content);

        return $dashboard;
    }

    /**
     * Added dynamic multifields simple functionality presentation
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function dynamicMultiFieldsSimple(Dashboard $dashboard): Dashboard
    {
        $content = view()->file(__DIR__ . '/../../../docs/js-scripts/dynamic-multifields-simple.md');

        $dashboard->page()
            ->setPageTitle('Dynamically adding multiFields simple JS script')
            ->addElement()
            ->box()
            ->makeSimple()
            ->content($content);

        return $dashboard;
    }
}
