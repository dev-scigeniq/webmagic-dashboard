<?php


namespace Scigeniq\Dashboard\Docs\Http;

use Faker\Generator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Scigeniq\Dashboard\Components\TablePageGenerator;
use Scigeniq\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Scigeniq\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Scigeniq\Dashboard\Dashboard;
use Scigeniq\Dashboard\Elements\Boxes\Box;
use Scigeniq\Dashboard\Elements\Tables\Table;
use Scigeniq\Dashboard\Services\DashboardSettingsManager;

class TablePresentationController
{
    /**
     * @return TablePageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function manualSortingDocs()
    {
        $content = view()->file(__DIR__ . '/../../../docs/tables/manual-sorting.md');
        $box = (new Box())->makeSimple()->content($content);

        $data = $this->prepareFakeData();
        $tablePageGenerator = (new TablePageGenerator())
            // Add items
            ->items($data)
            // Manual sorting activation
            ->manualSorting(
                url()->current(),
                function ($item) {
                    return $item['id'];
                },
                'GET'
            );

        $tablePageGenerator->getPage()->addContent($box, 'data', true);

        return $tablePageGenerator;
    }

    /**
     * Prepare fake data
     *
     * @return array
     */
    protected function prepareFakeData()
    {
        /** @var Generator $faker */
        $faker = app(Generator::class);
        $data = [];
        for ($i = 0; $i < 10; $i++) {
            $data[] = [
                'id'      => $faker->numberBetween(0, 100),
                'name'    => $faker->name,
                'address' => $faker->address,
            ];
        }

        return $data;
    }

    /**
     * Prepare fake data for paginator
     *
     * @return array
     */
    protected function prepareFakeDataForPaginator($countElements = 10, $page = 0, $totalElements = 0)
    {
        if ($countElements > $totalElements) {
            $countElements = $totalElements;
        }

        $firstItemNum = $page * $countElements - $countElements;

        if ($countElements * $page > $totalElements) {
            $countElements = $countElements * $page - (($countElements * ($page - 1)) + ($countElements * $page - $totalElements));
        }

        /** @var Generator $faker */
        $faker = app(Generator::class);
        $data = [];
        for ($i = 0; $i < $countElements; $i++) {
            $data[] = [
                '#'       => $firstItemNum + $i + 1,
                'id'      => $faker->numberBetween(0, 100),
                'name'    => $faker->name,
                'address' => $faker->address,
            ];
        }

        return $data;
    }

    /**
     * @param null $addressLike
     * @return array|Collection
     */
    private function filteredData($addressLike = null)
    {
        $preparedData = $this->getPreparedDataForPagination();

        if ($addressLike) {
            $preparedData = $this->findInData($addressLike, $preparedData);
        }

        return is_array($preparedData) ? $preparedData : $preparedData->toArray();
    }

    /**
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function pagination(Dashboard $dashboard)
    {
        $content = view()->file(__DIR__ . '/../../../docs/tables/pagination.md');
        $dashboard->page()->content((new Box($content))->makeSimple());
        $dashboard->title('Pagination');

        return $dashboard;
    }

    /**
     * @param DashboardSettingsManager $dashboardSettingsManager
     * @return TablePageGenerator|Table
     */
    public function paginationBaseUsing(DashboardSettingsManager $dashboardSettingsManager)
    {
        $perPage = $dashboardSettingsManager->getPaginationStep();

        $total = 500;
        $page = request()->input('page', 1);
        $data = $this->prepareFakeDataForPaginator($perPage, $page, $total);
        $paginator = new LengthAwarePaginator($data, $total, $perPage);

        $tableGenerator = (new TablePageGenerator())
            ->title('Base using pagination')
            ->tableTitles('#', 'ID', 'Name','Address')
            ->items($data)
            ->withPagination($paginator, request()->url());

        if (request()->ajax()) {
            return $tableGenerator->getTable();
        }

        return $tableGenerator;
    }


    /**
     * @param DashboardSettingsManager $dashboardSettingsManager
     * @return TablePageGenerator|Table
     */
    public function paginationBaseUsingWithDropdown(DashboardSettingsManager $dashboardSettingsManager)
    {
        $perPage = $dashboardSettingsManager->getPaginationStep();

        $total = 500;
        $page = request()->input('page', 1);
        $data = $this->prepareFakeDataForPaginator($perPage, $page, $total);
        $paginator = new LengthAwarePaginator($data, $total, $perPage);

        $tableGenerator = (new TablePageGenerator())
            ->title('Base using pagination with dropdown')
            ->tableTitles('#', 'ID', 'Name','Address')
            ->items($data)
            ->withPagination($paginator, request()->url(), true);

        if (request()->ajax()) {
            return $tableGenerator->getTable();
        }

        return $tableGenerator;
    }

    /**
     * @param DashboardSettingsManager $dashboardSettingsManager
     * @return TablePageGenerator|Table
     */
    public function paginationWithCustomPerPage(DashboardSettingsManager $dashboardSettingsManager)
    {
        $perPage = $dashboardSettingsManager->getPaginationStep(null, 'per_page', 30);

        $total = 500;
        $page = request()->input('page', 1);
        $data = $this->prepareFakeDataForPaginator($perPage, $page, $total);
        $paginator = new LengthAwarePaginator($data, $total, $perPage);

        $tableGenerator = (new TablePageGenerator())
            ->title('Pagination with custom per page dropdown')
            ->tableTitles('#', 'ID', 'Name','Address')
            ->items($data)
            ->withPagination($paginator, request()->url(), true, [30, 40, 50, 60]);

        if (request()->ajax()) {
            return $tableGenerator->getTable();
        }

        return $tableGenerator;
    }

    /**
     * @return \Scigeniq\Dashboard\Pages\BasePage
     * @throws NoOneFieldsWereDefined
     */
    public function paginationWithTwoTablesAndFilters()
    {
        $firstTable = $this->paginationFirstTableAndFilter(new DashboardSettingsManager());
        $secondTable = $this->paginationSecondTableAndFilter(new DashboardSettingsManager());

        $dashboard = (new Dashboard())
            ->title('Pages')
            ->contentHeader('<h3>Two tables and filters</h3>');

        $dashboard->addContent('<h4>First table</h4>');
        $dashboard->addContent($firstTable->getBox());
        $dashboard->addContent('<br>');
        $dashboard->addContent('<h4>Second table</h4>');
        $dashboard->addContent($secondTable->getBox());

        return $dashboard;
    }

    /**
     * @param $perPageItems
     * @param $pageNum
     * @param $address
     * @return array
     */
    private function dataPerPage($perPageItems, $pageNum, $address)
    {
        $filteredData = $this->filteredData($address);
        $firstItemNum = $pageNum * $perPageItems - $perPageItems;

        $data = [];
        for ($i = $firstItemNum, $len = $i + $perPageItems; $i < $len; $i++) {
            if(!isset($filteredData[$i])) {
                continue;
            }

            $data[] = array_prepend($filteredData[$i], $i + 1, '#');
        }

        return $data;
    }

    /**
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function paginationFirstTableAndFilter(DashboardSettingsManager $dashboardSettingsManager)
    {
        $paginationStepParam = 'per_page_first_table';
        $perPageItems = $dashboardSettingsManager->getPaginationStep('firstPage', $paginationStepParam, 3);

        $address = request()->input('address');
        $pageNum = request()->input('page', 1);
        $filteredDataCount = count($this->filteredData($address));
        $dataPerPage = $this->dataPerPage($perPageItems, $pageNum, $address);
        $paginator = new LengthAwarePaginator($dataPerPage, $filteredDataCount, $perPageItems, $pageNum);

        $tableGenerator = (new TablePageGenerator())
            ->tableTitles('#', 'ID', 'Name','Address')
            ->items($dataPerPage)
            ->withPagination(
                $paginator,
                route('dashboard.docs.presentation.tables.paginationFirstTableAndFilter'),
                true,
                [1, 2, 3, 4],
                $paginationStepParam
            )
        ;

        $tableGenerator
            ->addFiltering()
            ->action(route('dashboard.docs.presentation.tables.paginationFirstTableAndFilter'))
            ->textInput('address', $address, 'Address', false, ['placeholder' => '%like%'])
            ->submitButton('Search')
        ;

        if (request()->ajax()) {
            return $tableGenerator->getTable();
        }

        return $tableGenerator;
    }

    /**
     * @param DashboardSettingsManager $dashboardSettingsManager
     * @return TablePageGenerator|Table
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function paginationSecondTableAndFilter(DashboardSettingsManager $dashboardSettingsManager)
    {
        $paginationStepParam = 'per_page_second_table';
        $perPageItems = $dashboardSettingsManager->getPaginationStep('secondPage', $paginationStepParam, 1);

        $address = request()->input('address');
        $pageNum = request()->input('page', 1);
        $filteredDataCount = count($this->filteredData($address));
        $dataPerPage = $this->dataPerPage($perPageItems, $pageNum, $address);
        $paginator = new LengthAwarePaginator($dataPerPage, $filteredDataCount, $perPageItems, $pageNum);

        $tableGenerator = (new TablePageGenerator())
            ->tableTitles('#', 'ID', 'Name','Address')
            ->items($dataPerPage)
            ->withPagination(
                $paginator,
                route('dashboard.docs.presentation.tables.paginationSecondTableAndFilter'),
                true,
                [1, 2, 3, 4],
                $paginationStepParam
            )
        ;

        $tableGenerator
            ->addFiltering()
            ->action(route('dashboard.docs.presentation.tables.paginationSecondTableAndFilter'))
            ->textInput('address', $address, 'Address', false, ['placeholder' => '%like%'])
            ->submitButton('Search')
        ;

        if (request()->ajax()) {
            return $tableGenerator->getTable();
        }

        return $tableGenerator;
    }

    /**
     * @return Collection
     */
    private function getPreparedDataForPagination(): Collection
    {
        return collect([
            ['id' => '21321', 'name' => 'Prof. Antonina Schultz V', 'address' => '189 Mann Courts Suite 434 Joanyport, KS 58233'],
            ['id' => '321323', 'name' => 'Krystel Dooley', 'address' => '8041 Hillary Centers Suite 426 Smithburgh, WY 36129-9160'],
            ['id' => '546456', 'name' => 'Mr. Terry Rohan IV', 'address' => '75247 McGlynn Ramp Apt. 465 Domenicafurt, AL 36588-9145'],
            ['id' => '54', 'name' => 'Lily Miller', 'address' => '1197 Schiller Common Apt. 687 South Lea, OH 32543-9551'],
            ['id' => '22', 'name' => 'Gerson Stiedemann Sr.', 'address' => '4880 Weber Causeway West Kaelyn, NC 33083-6268  '],
            ['id' => '74', 'name' => 'Karson Ullrich III', 'address' => '90084 Jaycee Islands West Pink, KY 03718'],
            ['id' => '720', 'name' => 'Andres Bernier', 'address' => '252 Zoila Run Apt. 754 West Neilside, NV 71129'],
            ['id' => '4982', 'name' => 'Ethan Thompson MD', 'address' => '973 Shyann Light Helgamouth, OR 14265-2237'],
            ['id' => '4358', 'name' => 'Jalen Walter', 'address' => '165 Considine Squares Suite 372 North Boyd, OR 24878'],
            ['id' => '222', 'name' => 'Kylee Cummings PhD', 'address' => '13094 Edna Union South Mercedes, CT 99386'],
            ['id' => '797', 'name' => 'Katelyn McDermott', 'address' => '884 Bradtke Neck Apt. 780 Port Estelleside, NE 24315'],
            ['id' => '3845', 'name' => 'Karli Jacobson', 'address' => '3138 Willms Orchard Prohaskachester, RI 79145'],
            ['id' => '95564', 'name' => 'Ofelia Schuppe', 'address' => '902 Renner Villages Torpfurt, MI 22370'],
            ['id' => '12447', 'name' => 'Catharine Beatty', 'address' => '1793 Elizabeth Glens Lake Jacinthe, ND 73538'],
            ['id' => '57566', 'name' => 'Ernesto Smitham III', 'address' => '75389 Yundt Skyway Apt. 626 North Elmer, TN 88321'],

        ]);
    }

    /**
     * @param $search
     * @param array $data
     * @return array
     */
    private function findInData(string $search, Collection $data): array
    {
        $data = $data->filter(function ($item) use ($search) {
            if(strpos($item['address'], $search) !== false) {
                return $item;
            }
        });

        return array_values($data->toArray());
    }


}
