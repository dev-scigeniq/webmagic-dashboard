<?php


namespace Scigeniq\Dashboard\Core;

use Scigeniq\Dashboard\Core\Content\AttributesAvailable;
use Scigeniq\Dashboard\Core\Content\ContentFieldsUsable;
use Scigeniq\Dashboard\Core\Content\JsActionsApplicable;
use Scigeniq\Dashboard\Core\Content\JsActionsApplicableTrait;
use Scigeniq\Dashboard\Elements\Factories\CreatesElements;
use Scigeniq\Dashboard\Elements\Factories\CreateElementsTrait;
use Scigeniq\Dashboard\Core\Content\ContentFieldsUsableTrait;
use Scigeniq\Dashboard\Core\View\ViewUsable;

class ComplexElement extends AbstractComplexRenderableElement implements
    ContentFieldsUsable,
    CreatesElements,
    AttributesAvailable,
    JsActionsApplicable
{
    use ViewUsable, ContentFieldsUsableTrait, CreateElementsTrait, JsActionsApplicableTrait;

    /**
     * Return all the data which need for render the view
     *
     * @return array
     * @throws Content\Exceptions\NoOneFieldsWereDefined
     */
    public function getViewData(): array
    {
        return $this->toArray();
    }
}
