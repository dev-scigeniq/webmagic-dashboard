<?php

namespace Scigeniq\Dashboard\Core\Content;


use Scigeniq\Dashboard\JsActions\JsActionsCollection;

interface JsActionsApplicable extends ClassAvailable, AttributesAvailable
{
    /**
     * Start js action applaying
     *
     * @return JsActionsCollection
     */
    public function js(): JsActionsCollection;
}
