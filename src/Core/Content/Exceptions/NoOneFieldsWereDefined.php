<?php


namespace Scigeniq\Dashboard\Core\Content\Exceptions;

use Scigeniq\Dashboard\Pages\Page;

class NoOneFieldsWereDefined extends \Exception
{
    /**
     * NoOneFieldWereDefined constructor.
     *
     * @param Page $object
     */
    public function __construct($object)
    {
        $className = get_class($object);
        $message = "No one field was defined in $className" ;
        parent::__construct($message);
    }
}
