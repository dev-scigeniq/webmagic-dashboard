<?php


namespace Scigeniq\Dashboard\Core\Content;


use Scigeniq\Dashboard\JsActions\JsActionsCollection;

trait JsActionsApplicableTrait
{
    /**
     * @return JsActionsCollection
     */
    public function js(): JsActionsCollection
    {
        return new JsActionsCollection($this);
    }
}
