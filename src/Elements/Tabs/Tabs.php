<?php


namespace Scigeniq\Dashboard\Elements\Tabs;

use Scigeniq\Dashboard\Core\ComplexElement;
use Scigeniq\Dashboard\Core\Content\ContentFieldsUsableTrait;
use Scigeniq\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Scigeniq\Dashboard\Elements\Factories\CreatesElements;

/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Tabs\Tabs tabs(Scigeniq\Dashboard\Elements\Tabs\Tab $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tabs\Tabs addTabs(Scigeniq\Dashboard\Elements\Tabs\Tab $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tabs\Tabs navigation(Scigeniq\Dashboard\Elements\Tabs\Navigation $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tabs\Tabs addNavigation(Scigeniq\Dashboard\Elements\Tabs\Navigation $valueOrConfig)
 *
 ********************************************************************************************************************/

class Tabs extends ComplexElement
{
    /** @var  string Component view name */
    protected $view = 'dashboard::elements.tabs.tabs';

    /** @var  array Sections available in page */
    protected $available_fields = [
        'tabs' => [
            'type' => Tab::class,
            'array_acceptable' => true
        ],
        'navigation' => [
            'type' => Navigation::class
        ]
    ];

    /** @var  string Default section for current component */
    protected $default_field = 'tabs';

    /**
     * Prepare navigation if not set
     *
     * @return CreatesElements|Navigation|ContentFieldsUsableTrait
     * @throws \Scigeniq\Dashboard\Core\Content\NoOneFieldsWereDefined
     * @throws NoOneFieldsWereDefined
     */
    public function getNavigation()
    {
        if (isset($this->navigation) || !isset($this->tabs)) {
            return $this->navigation;
        }

        return (new Navigation())->navigationTabs($this->tabs);
    }

    /**
     * Add new tab and return it
     *
     * @return Tab
     */
    public function addTab()
    {
        return $this->addElement()->tab();
    }
}
