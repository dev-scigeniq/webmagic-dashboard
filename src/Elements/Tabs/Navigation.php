<?php


namespace Scigeniq\Dashboard\Elements\Tabs;

use Scigeniq\Dashboard\Core\ComplexElement;

/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Tabs\Navigation class($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tabs\Navigation addClass($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tabs\Navigation navigationTabs(Scigeniq\Dashboard\Elements\Tabs\Tab $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tabs\Navigation addNavigationTabs(Scigeniq\Dashboard\Elements\Tabs\Tab $valueOrConfig)
 *
 ********************************************************************************************************************/

class Navigation extends ComplexElement
{
    /** @var  string Component view name */
    protected $view = 'dashboard::elements.tabs.navigation';

    /** @var  array Sections available in page */
    protected $available_fields = [
        'class',
        'navigation_tabs' => [
            'type' => Tab::class,
            'array_acceptable' => true
        ]
    ];

    /** @var  string Default section for current component */
    protected $default_field = 'classes';
}
