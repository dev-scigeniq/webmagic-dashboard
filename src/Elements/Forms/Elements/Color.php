<?php


namespace Scigeniq\Dashboard\Elements\Forms\Elements;

use Scigeniq\Dashboard\Core\ComplexElement;


/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Color id($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Color addId($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Color class($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Color addClass($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Color type($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Color addType($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Color required(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Color addRequired(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Color value($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Color addValue($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Color placeholder($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Color addPlaceholder($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Color name($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Color addName($valueOrConfig)
 *
 ********************************************************************************************************************/

class Color extends Input
{
    /** @var  string Component view name */
    protected $view = 'dashboard::components.form.elements.color';

    /** @var  array Sections available in page */
    protected $available_fields = [
        'id',
        'class'    => [
            'default' => 'js-color-pick',
        ],
        'type'     => [
            'default' => 'text',
        ],
        'required' => [
            'type'    => 'bool',
            'default' => false,
        ],
        'value'    => [
            'default' => 'ffffff',
        ],
        'placeholder',
        'name'
    ];

    /** @var  string Default section for current component */
    protected $default_field = 'value';
}
