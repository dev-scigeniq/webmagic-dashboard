<?php


namespace Scigeniq\Dashboard\Elements\Forms\Elements;

use Illuminate\Support\Carbon;
use Scigeniq\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;

/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\DateInput id($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\DateInput addId($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\DateInput class($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\DateInput addClass($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\DateInput type($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\DateInput addType($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\DateInput required(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\DateInput addRequired(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\DateInput value($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\DateInput addValue($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\DateInput placeholder($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\DateInput addPlaceholder($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\DateInput name($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\DateInput addName($valueOrConfig)
 *
 ********************************************************************************************************************/

class DateInput extends Input
{
    /**
     * DateInput constructor.
     *
     * @param null $content
     *
     * @throws NoOneFieldsWereDefined
     */
    public function __construct($content = null)
    {
        $this->type = 'date';

        parent::__construct($content);
    }

    /**
     * Convert date to correct format
     *
     * @param $value
     */
    protected function setValue($value)
    {
        $this->value = Carbon::parse($value)->toDateString();
    }
}
