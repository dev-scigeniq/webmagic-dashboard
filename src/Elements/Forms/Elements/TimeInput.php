<?php


namespace Scigeniq\Dashboard\Elements\Forms\Elements;

use Illuminate\Support\Carbon;
use Scigeniq\Dashboard\Core\Content\NoOneFieldsWereDefined;

/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\TimeInput id($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\TimeInput addId($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\TimeInput class($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\TimeInput addClass($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\TimeInput type($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\TimeInput addType($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\TimeInput required(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\TimeInput addRequired(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\TimeInput value($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\TimeInput addValue($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\TimeInput placeholder($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\TimeInput addPlaceholder($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\TimeInput name($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\TimeInput addName($valueOrConfig)
 *
 ********************************************************************************************************************/

class TimeInput extends Input
{
    /**
     * Set type file
     *
     * TimeInput constructor.
     * @param null $content
     * @throws NoOneFieldsWereDefined
     */
    public function __construct($content = null)
    {
        $this->type = 'time';

        parent::__construct($content);
    }

    /**
     * Convert value to correct time string
     *
     * @param $value
     * @return string
     */
    protected function setValue($value)
    {
        $this->value = Carbon::parse($value)->toTimeString();
    }
}
