<?php


namespace Scigeniq\Dashboard\Elements\Titles;

use Scigeniq\Dashboard\Core\ComplexElement;

/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Titles\H1Title title($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Titles\H1Title addTitle($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Titles\H1Title subTitle($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Titles\H1Title addSubTitle($valueOrConfig)
 *
 ********************************************************************************************************************/

class H1Title extends ComplexElement
{
    protected $view = 'dashboard::elements.titles.h1_title';

    protected $available_fields = [
        'title',
        'sub_title'
    ];

    protected $default_field = 'title';
}
