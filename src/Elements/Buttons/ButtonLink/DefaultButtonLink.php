<?php


namespace Scigeniq\Dashboard\Elements\Buttons\ButtonLink;

use Scigeniq\Dashboard\Core\ComplexElement;

class DefaultButtonLink extends ComplexElement
{
    protected $view = 'dashboard::elements.buttons.button_link';

    protected $available_fields = [
        'content',
        'class' => [
            'default' => 'btn-primary'
        ],
        'iconFirst',
        'iconLast',
//        'name'
    ];

    protected $default_field = 'content';
}
