<?php

namespace Scigeniq\Dashboard\Elements\Buttons\ButtonGroup;

use Scigeniq\Dashboard\Core\ComplexElement;

/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Buttons\ButtonGroup\ButtonGroup class($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\ButtonGroup\ButtonGroup addClass($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\ButtonGroup\ButtonGroup buttonClass($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\ButtonGroup\ButtonGroup addButtonClass($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\ButtonGroup\ButtonGroup link($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\ButtonGroup\ButtonGroup addLink($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\ButtonGroup\ButtonGroup content($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\ButtonGroup\ButtonGroup addContent($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\ButtonGroup\ButtonGroup icon($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\ButtonGroup\ButtonGroup addIcon($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\ButtonGroup\ButtonGroup items(array $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\ButtonGroup\ButtonGroup addItems(array $valueOrConfig)
 *
 ********************************************************************************************************************/

class ButtonGroup extends ComplexElement
{
    protected $view = 'dashboard::elements.buttons.button_group.button_group';

    protected $available_fields = [
        'class',
        'button_class' => [
            'default' => 'btn-info'
        ],
        'link',
        'content',
        'icon',
        'items' => [
            'type' => 'array',
            'default' => []
        ]
    ];

    protected $default_field = 'content';
}
