<?php

namespace Scigeniq\Dashboard\Elements\Buttons\ButtonGroup;

use Scigeniq\Dashboard\Core\ComplexElement;

class ButtonGroupDivider extends ComplexElement
{
    protected $view = 'dashboard::elements.buttons.button_group.button_group_divider';

    protected $available_fields = [
        'class'
    ];

    protected $default_field = 'class';
}
