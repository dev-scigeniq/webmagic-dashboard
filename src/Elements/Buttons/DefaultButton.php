<?php


namespace Scigeniq\Dashboard\Elements\Buttons;

use Scigeniq\Dashboard\Core\ComplexElement;

/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Buttons\DefaultButton type($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\DefaultButton addType($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\DefaultButton content($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\DefaultButton addContent($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\DefaultButton class($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\DefaultButton addClass($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\DefaultButton icon($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Buttons\DefaultButton addIcon($valueOrConfig)
 *
 ********************************************************************************************************************/

class DefaultButton extends ComplexElement
{
    protected $view = 'dashboard::elements.buttons.button';

    protected $available_fields = [
        'type' => [
            'default' => 'button',
        ],
        'content',
        'class' => [
            'default' => 'btn-primary'
        ],
        'icon'
    ];

    protected $default_field = 'content';
}
