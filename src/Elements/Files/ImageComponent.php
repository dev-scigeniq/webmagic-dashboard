<?php


namespace Scigeniq\Dashboard\Elements\Files;


/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent imgUrl(string $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent addImgUrl(string $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent fileName($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent addFileName($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent downloadUrl($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent addDownloadUrl($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent size($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent addSize($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent width($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent addWidth($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent height($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent addHeight($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent title($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent addTitle($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent name($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent addName($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent action($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent addAction($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent method($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent addMethod($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent id($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent addId($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent deleteAction($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent addDeleteAction($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent deleteMethod($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent addDeleteMethod($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent noCaching(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\ImageComponent addNoCaching(bool $valueOrConfig)
 *
 ********************************************************************************************************************/

class ImageComponent extends ImagePreview
{
    /** @var  string Component view name */
    protected $view = 'dashboard::elements.files.image_component';

    /** @var  array Sections available in page */
    protected $available_fields = [
        'img_url' => [
            'type' => 'string'
        ],
        'file_name',
        'download_url',
        'size',
        'width',
        'height',
        'title',
        'name',
        'action',
        'method' => [
            'default' => 'POST'
        ],
        'id',
        'delete_action',
        'delete_method' => [
            'default' => 'POST'
        ],
        'no_caching' => [
            'type' => 'bool',
            'default' => true
        ]
    ];

    /** @var  string Default section for current component */
    protected $default_field = 'img_url';

    /**
     * Clean up default image
     */
    protected function setDefaultImage()
    {
        $this->available_fields['img_url']['default'] = '';
    }

    /**
     * ImageComponent constructor.
     *
     * @param null $content
     *
     * @throws \Scigeniq\Dashboard\Core\Content\Exceptions\FieldUnavailable
     * @throws \Scigeniq\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined
     */
    public function __construct($content = null)
    {
        parent::__construct($content);

        $this->param('id', uniqid());
    }


}
