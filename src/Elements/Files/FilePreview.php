<?php

namespace Scigeniq\Dashboard\Elements\Files;


use Illuminate\Http\File;
use Scigeniq\Dashboard\Core\ComplexElement;
use Scigeniq\Dashboard\Elements\StringElement;


/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Files\FilePreview icon($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\FilePreview addIcon($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\FilePreview name($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\FilePreview addName($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\FilePreview downloadUrl($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\FilePreview addDownloadUrl($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\FilePreview size($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Files\FilePreview addSize($valueOrConfig)
 *
 ********************************************************************************************************************/

class FilePreview extends ComplexElement
{
    /** @var  StringElement Component view name */
    protected $view = 'dashboard::elements.files.file_preview';

    /** @var  array Sections available in page */
    protected $available_fields = [
        'icon' => [
            'default' => 'fas fa-file-o'
        ],
        'name',
        'download_url',
        'size'
    ];

    /** @var  StringElement Default section for current component */
    protected $default_field = 'download_url';


    /**
     * @return string|\Scigeniq\Dashboard\Core\Content\ContentFieldsUsableTrait|\Scigeniq\Dashboard\Elements\Factories\CreatesElements
     * @throws \Scigeniq\Dashboard\Core\Content\NoOneFieldsWereDefined
     */
    public function getFileName()
    {
        if(isset($this->file_name)){
            return $this->file_name;
        }

        return basename($this->getFieldValue('download_url'));
    }

    /**
     * Initiate with file
     *
     * @param File $file
     */
    public function setFile(File $file)
    {
        $this->file_name = $file->getFilename().$file->getExtension();

        $this->size = $file->getSize();
    }
}
