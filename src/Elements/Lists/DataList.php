<?php

namespace Scigeniq\Dashboard\Elements\Lists;


use Scigeniq\Dashboard\Core\ComplexElement;
use Scigeniq\Dashboard\Elements\StringElement;


/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Lists\DataList data(array $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Lists\DataList addData(array $valueOrConfig)
 *
 ********************************************************************************************************************/

class DataList extends ComplexElement
{
    /** @var  StringElement Component view name */
    protected $view = 'dashboard::elements.lists.description-block';

    /** @var  array Sections available in page */
    protected $available_fields = [
        'data'          => [
            'type'    => 'array',
            'default' => [],
        ],
    ];

    /** @var  StringElement Default section for current component */
    protected $default_field = 'data';
}