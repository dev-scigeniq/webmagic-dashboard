<?php


namespace Scigeniq\Dashboard\Elements;


use Scigeniq\Dashboard\Core\ComplexElement;


/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\WrapperDiv content($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\WrapperDiv addContent($valueOrConfig)
 *
 ********************************************************************************************************************/

class WrapperDiv extends ComplexElement
{
    /** @var  StringElement Component view name */
    protected $view = 'dashboard::elements.div';

    /** @var  array Sections available in page */
    protected $available_fields = [
        'content'
    ];

    /** @var  StringElement Default section for current component */
    protected $default_field = 'content';
}
