<?php


namespace Scigeniq\Dashboard\Elements\Tables;

use Exception;
use Scigeniq\Dashboard\Core\ComplexElement;
use Scigeniq\Dashboard\Core\Utils;
use Scigeniq\Dashboard\Elements\Paginator;

/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle title($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle addTitle($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle sortBy($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle addSortBy($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle nextSort($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle addNextSort($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle addAction($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle method($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle addMethod($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle resultReplaceBlockClass($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle addResultReplaceBlockClass($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle currentSort($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle addCurrentSort($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle noneSortingAvailable(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle addNoneSortingAvailable(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Tables\TableTitle sortAvailable(bool $valueOrConfig)
 *
 ********************************************************************************************************************/

class TableTitle extends ComplexElement
{
    /** @var  string Component view name */
    protected $view = 'dashboard::components.tables.table_title';

    /** @var  array Sections available in page */
    protected $available_fields = [
        'title',
        'sort_by',
        'next_sort' => [
            'default' => 'asc',
            'acceptable_values' => [
                'asc',
                'desc',
                ''
            ]
        ],
        'action',
        'method' => [
            'default' => 'GET'
        ],
        'result_replace_block_class',
        'current_sort' => [
            'default' => 'none',
            'acceptable_values' => [
                'asc',
                'desc',
                'none'
            ]
        ],
        'none_sorting_available' => [
            'type' => 'bool',
            'default' => true
        ],
        'sort_available' => [
            'type' => 'bool',
            'default' => true
        ]
    ];

    /** @var  string Default section for current component */
    protected $default_field = 'title';

    /**
     * Set next sort value
     *
     * @param $value
     * @return TableTitle
     * @throws Exception
     */
    protected function setCurrentSort($value)
    {
        $this->isFieldValid('current_sort', $value);

        $this->current_sort = $value;

        if ($value == 'asc') {
            $this->next_sort = 'desc';
        } elseif ($value == 'desc' && $this->getFieldValue('none_sorting_available')) {
            $this->nextSort = '';
        } else {
            $this->next_sort = 'asc';
        }

        return $this;
    }

    /**
     * Add action for sending request
     *
     * @param string $action
     * @param bool   $includeRequestQuery
     *
     * @return $this
     */
    public function action(string $action, bool $includeRequestQuery = true)
    {
        if($includeRequestQuery){
            $action = Utils::appendsAllRequestToURL($action, ['sort', 'sortBy', '_token', '_method']);
        }

        $this->action = $action;

        return $this;
    }
}
