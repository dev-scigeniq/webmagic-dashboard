<?php


namespace Scigeniq\Dashboard\JsActions;


class SendRequestOnChange extends SendRequestOnClick
{
    /** @var string  */
    protected $actionClass = 'js_ajax-by-change';
}
