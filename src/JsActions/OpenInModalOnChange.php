<?php


namespace Scigeniq\Dashboard\JsActions;


class OpenInModalOnChange extends OpenInModalOnClick
{
    protected $actionClass = 'js_ajax-by-change';
}
