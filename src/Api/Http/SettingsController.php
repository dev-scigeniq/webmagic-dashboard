<?php


namespace Scigeniq\Dashboard\Api\Http;

use Illuminate\Http\Request;
use Scigeniq\Dashboard\Services\DashboardSettingsManager;

/**
 * Class SettingsController
 * @package Scigeniq\Dashboard\Api\Http
 */
class SettingsController
{
    /**
     * @param Request $request
     * @param DashboardSettingsManager $dashboardSettingsManager
     */
    public function setLocale(Request $request, DashboardSettingsManager $dashboardSettingsManager)
    {
        $lang = $request->post('lang');
        if (collect(config('scigeniq.dashboard.dashboard.header_navigation_options.available_locales'))->contains('code', $lang)) {
            $dashboardSettingsManager->setLocale($lang);
        }
    }
}