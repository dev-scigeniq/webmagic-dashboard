<?php

namespace Scigeniq\Dashboard\Pages;

use Scigeniq\Dashboard\Core\Content\ContentFieldsUsable;
use Scigeniq\Dashboard\Core\Content\ContentFieldsUsableTrait;
use Scigeniq\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Scigeniq\Dashboard\Core\Content\JsActionsApplicable;
use Scigeniq\Dashboard\Core\Content\JsActionsApplicableTrait;
use Scigeniq\Dashboard\Elements\Factories\CreatesElements;
use Scigeniq\Dashboard\Elements\Factories\CreateElementsTrait;
use Scigeniq\Dashboard\Core\View\ViewUsable;

class Page extends AbstractPage implements ContentFieldsUsable, CreatesElements, JsActionsApplicable
{
    use ViewUsable, ContentFieldsUsableTrait, CreateElementsTrait, JsActionsApplicableTrait;

    /**
     * Return all the data which need for render the view
     *
     * @return array
     * @throws NoOneFieldsWereDefined
     */
    public function getViewData(): array
    {
        return $this->toArray();
    }
}
