Functionality allows you to send a request and, if necessary, display a response.  
  
The functional is implemented using  
``\Scigeniq\Dashboard\JsActions\SendRequestOnClick`` class and  
``\Scigeniq\Dashboard\JsActions\SendRequestOnChange`` class

```php
$button = new DefaultButton('Click');
$button->js()->sendRequestOnClick()->regular(
    $route, 
    $paramsForRequest = [], 
    $method = 'POST', 
    $successNotification = true, 
    $errorNotification = false, 
    $reloadOnSuccess = false
);
``` 

After clicking button, request will be executed
* to route ``$route`` 
* using the ``POST`` method 
* with additional ``$paramsForRequest`` parameters 
* notifying the user about the successful execution of the request
* without notifying about the error of the request
* and without reloading the page.

You can also output the result to a block.  
For it use ``showResponse`` function:  
```php
$button = new DefaultButton('Click');
$button->js()->sendRequestOnClick()->showResponse(
    $routeWithResultBlock, 
    $classOfBlockForShowingResult = '.result-block', 
    $paramsForRequest = ['some' => 'Value for result block.'],
    $successNotification = true, 
    $errorNotification = false, 
    $reloadOnSuccess = false
);
``` 
  
Also, you can execute request with replace target block.  
For it use ``replaceWithResponse`` function:  
```php
$button = new DefaultButton('Click');
$button->js()->sendRequestOnClick()->replaceWithResponse(
    $routeWithResultBlock, 
    $classOfBlockForReplaceByResponse = '.replace-block', 
    $paramsForRequest = ['some' => 'Value for replace block.'],
    $successNotification = true, 
    $errorNotification = false, 
    $reloadOnSuccess = false
);
``` 
  
For a more detailed example, see ``\Scigeniq\Dashboard\Docs\Http\JSActionsPresentationController::sendRequest``  