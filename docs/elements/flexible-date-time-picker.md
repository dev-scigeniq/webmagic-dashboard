## FlexibleDateTimePicker


Additional element allows to select only ``month and year`` in datapicker or only ``year``.

### Example
```php
$flexibleDateTimePicker = (new FlexibleDateTimePicker())
        ->name('flexible_datetime_picker')
        ->value(now())
        ->horizontalPosition('right')
        ->verticalPosition('bottom')
        ->showWeekNumber(false)
        ->toolbarPosition('top')
        ->minYear(now()->subYears(5)->year)
        ->maxYear(now()->addYears(5)->year)
        ->viewMode('months')
        ->setDateFormat('Y-m');
```