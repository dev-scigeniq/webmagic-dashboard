Functionality to easily implement [bootstrap grid](https://getbootstrap.com/docs/5.0/layout/grid/)  
The functional is implemented using `\Scigeniq\Dashboard\Elements\Grid\Grid`  
  
```php
$grid = (new Grid());
$grid->addContent([1,2,3,4,5,6...]);
```
You can add elements by row, and it will automate wrap columns depending on window size.  
Also, you can set columns count by window size using functions:
```php
$grid->xsRowCount($default = 1);
$grid->smRowCount($default = 1);
$grid->mdRowCount($default = 4);
$grid->lgRowCount($default = 6);
```
  
Also, you can set data before and after grid:
```php
$grid->beforeGrid('some data before grid');
$grid->afterGrid('some data after grid');
```
  
By using element Grid, you can build forms like so:
```php
(new FormPageGenerator())->getBox()->element()->grid([
    (new Box())
        ->showFullscreenBtn()
        ->boxHeaderContent('<b>From</b>')
        ->content([
            (new FormGroup())->select('outgoing_account_id', [1 => 'Option 1', 2 => 'Option 2'], '', 'Account', true),
            (new FormGroup())->dateInput('outgoing_date', now(), 'Date', true),
            (new FormGroup())->numberInput('value', 0, 'Value', true, 0.01),
            (new FormGroup())->numberInput('outgoing_commission', 0, 'Commission', false, 0.01),
            (new FormGroup())->select('outgoing_contractor_id', [1 => 'Option 1', 2 => 'Option 2'], '', 'Contractor'),
        ]),
    (new Box())
        ->boxHeaderContent('&nbsp')
        ->content([
            (new FormGroup())->numberInput('rate', 1, 'Rate', true, 0.01),
            (new FormGroup())->textarea('comment', '', 'Comment', false, ['rows' => 8]),
        ]),
    (new Box())
        ->boxHeaderContent('<b>To</b>')
        ->content([
            (new FormGroup())->select('incoming_account_id', [1 => 'Option 1', 2 => 'Option 2'], '', 'Account', true),
            (new FormGroup())->datePickerJS('incoming_date', now(), 'Date', true),
            (new FormGroup())->numberInput('value', 0, 'Value', true, 0.01),
            (new FormGroup())->numberInput('incoming_commission', 0, 'Commission', false, 0.01),
            (new FormGroup())->select('incoming_contractor_id', array_prepend([1 => 'Option 1', 2 => 'Option 2'], '', ''), '', 'Contractor'),
        ]),
])->lgRowCount(3);
```
For a more detailed example, see `\Scigeniq\Dashboard\Docs\Http\PresentationController::grid`
