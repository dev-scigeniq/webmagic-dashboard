An element that allows you to add a label and focus on this.  
The functional is implemented using `\Scigeniq\Dashboard\Elements\Badge` class  
```php
$badge = new Badge('You have new messages');
$badge->addClass('badge-info');
```
For a more detailed example, see `\Scigeniq\Dashboard\Docs\Http\PresentationController::badge`  