### Link
Element allows you to create a simple link.  
The functional is implemented using the `\Scigeniq\Dashboard\Elements\Links\Link` class  
```php
$linkElement = new Link('link to Google search'));
$linkElement->link('https://www.google.com/');
$linkElement->inNewTab($default = true);
```  
Method **inNewTab** allows open links in a new tab.  

### LinkButton
Element allows you to create a link in the form of a button  
The functional is implemented using the `\Scigeniq\Dashboard\Elements\Links\LinkButton` class  
```php
$linkButton = new LinkButton('link button to DuckDuckGo search'));
$linkButton->link('https://duckduckgo.com/');
$linkButton->class('btn-warning');
$linkButton->icon('fa-link');
$linkButton->inNewTab();
```  
This element additional has method **icon**.  
Method takes class of icon from [Font Awesome](http://fontawesome.com/) library  

### LinkJSDelete
Element allows you to send a request to the server, as well as delete an HTML element.  
The functional is implemented using the `\Scigeniq\Dashboard\Elements\Links\LinkJSDelete` class  
```php
$linkJSDelete = (new LinkJSDelete('Remove rectangle'));
$linkJSDelete
    ->requestUri('/')
    ->method('GET')
    ->icon('fa-trash-alt')
    ->itemClass('rectangle')
    ->addClasses(' btn-danger')
    ->setModalTitle('Rectangle deleting')
    ->setModalContent('Are you sure you want to delete Rectangle?');

$rectangle = new WrapperDiv('Rectangle');
$rectangle->addClass('border rectangle');
```
The functionality of this element is similar with [Delete with confirmation](/dashboard/tech/js-actions/delete-with-confirmation) but implemented in a different way.  
* **requestUri** - route for request
* **method** - request method
* **icon** - takes an icon class that is added before the content of the button
* **itemClass** - class of the element that will be removed if the action is confirmed
* **addClasses** - additional classes of **LinkJSDelete**
* **setModalTitle** - modal title text
* **setModalContent** - modal content text

For a more detailed example, see `\Scigeniq\Dashboard\Docs\Http\PresentationController::links`