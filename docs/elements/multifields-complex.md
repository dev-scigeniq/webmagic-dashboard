## Multifields simple
``` \Scigeniq\Dashboard\Elements\Forms\MultifieldsElements\MultifieldsComplexElement```

The main difference from MultifieldsSimpleElement is the use of entity identifiers.  
MultifieldsComplexElement added:  
- required entity name in constructor
- method ``setAddButtonUrl`` with 2 parameters - link, method for sending data. An identifier should be returned in response.
- ``setRemoveButtonUrl`` method with 2 parameters - link, method for sending data.
- extended ``setData`` method  

The second parameter in ``setData`` - name of the identifier key [default = 'id'].  
It is needed in order to set the entity identifier in the database when initializing data


### How to use

```php
$dashboard = new Scigeniq\Dashboard\Dashboard();

$multifieldsComplexElement = new \Scigeniq\Dashboard\Elements\Forms\MultifieldsElements\MultifieldsSimpleElement();
$multifieldsComplexElement->setAddButtonUrl('/dashboard/tech/multifields-add', 'POST');
$multifieldsComplexElement->setRemoveButtonUrl('/dashboard/tech/multifields-delete', 'DELETE');
$multifieldsComplexElement->maxCopyCount(7);

$multifieldsComplexElement->addFields(
    (new Scigeniq\Dashboard\Elements\Forms\Elements\Input())->name('address'),
    (new Scigeniq\Dashboard\Elements\Forms\Elements\Textarea())->name('textarea'),
    (new Scigeniq\Dashboard\Elements\Forms\Elements\DateRange())->name('dateRange'),
    (new Scigeniq\Dashboard\Elements\Forms\Elements\DateTimePicker())->name('dateTimePicker'),
    (new \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS())->name('selectJS')->options([1,2,3]),
    (new \Scigeniq\Dashboard\Elements\Forms\Elements\Select)->name('select')->options([1,2,3])
);

$multifieldsComplexElement->setData(
    [
        [
            'idKey' => 12312312,
            'address' => 'address1',
            'textarea' => 'textarea2',
            'dateRange' => now(),
            'dateTimePicker' => now()->addDay(),
            'selectJS' => ['a' => 123, 'b', 'c'],
            'select' => ['a' => 456, 'b' => 789, 'c'],
        ],
        [
            'idKey' => 35435435,
            'address' => 'address2',
            'textarea' => 'textarea2',
            'dateRange' => now()->addDays(5),
            'dateTimePicker' => now()->addDays(6),
            'selectJS' => ['c', 'd', 'e'],
            'select' => ['a' => 101112, 'b' => 131415, 'c'],
        ],
    ],
    'idKey'
);

$formGenerator = new \Scigeniq\Dashboard\Components\FormGenerator();
$formGenerator->action('/dashboard/tech/multifields-test');
$formGenerator->getForm()->addContent($multifieldsSimpleElement);
$formGenerator->addSubmitButton([],'Submit', 'btn btn-primary float-left ml-2');

$dashboard->addContent($formGenerator);

return $dashboard;
```  
