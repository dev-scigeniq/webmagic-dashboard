# Notifications

## Create
You can create a notification in default way on page
```php
$page = new \Scigeniq\Dashboard\Pages\BasePage();
$page->showNotification('Title', 'Text', true, 'info', 'fas fa-info', false, null);
```
Or you can use element factory
```php
$something->addElement('notification_area')
            ->notification()->title(string)->text(string)->button(bool)->type(string)->icon(string)->autoHide(bool)->autoHideDelay(int);
```

## Quick usage
You can set few types of notifications in quick way: warning, danger, info & success
```php
$page->showWarningNotification('Title', 'Text', $closeButton = true, $autoHide = false, $autoHideDelay = null);
$page->showDangerNotification('Title', 'Text', $closeButton = true, $autoHide = false, $autoHideDelay = null);
$page->showInfoNotification('Title', 'Text', $closeButton = true, $autoHide = false, $autoHideDelay = null);
$page->showSuccessNotification('Title', 'Text', $closeButton = true, $autoHide = false, $autoHideDelay = null);
```

## Available fields
Notification has getters and setters for the next fields:
* title     - (string)
* text      - (string)
* button    - (bool)
* type      - (null|string)
* icon      - (null|string)

## Available types of notifications & default icons
```php
'info'      => 'fas fa-info',
'danger'    => 'fas fa-ban',
'warning'   => 'fas fa-exclamation-circle',
'success'   => 'fas fa-check'
```
You can also use another icons


# Notification Service
### NotificationService is a singleton! It's important!
You can push notification to massage bag from everywhere of your code. It will be displayed in global notification area on the page. 
```php
$service = app()->make(Scigeniq\Dashboard\NotificationService\NotificationService::class);
$service->addMessage('warning', 'Global note', $group = 'global');
``` 
In third argument you can pass prefix name for storing messages in Cache.  
When you use one of methods of \Scigeniq\Dashboard\NotificationService\NotificationService:
```php
public function getAllMessages(string $group = null, bool $saveForLater = false): array
public function getFirstMessage(string $type = '*', string $group = 'global', bool $saveForLater = false): string
public function getGroup(string $group, string $type = '*', bool $saveForLater = false): array
public function getByType(string $type, string $group = 'global', bool $saveForLater = false): array
```
You can pass last argument as bool ``true`` for saving message(s) after getting. If you use default value, message will be removed from storage.

Also, you can disable global notifications by method turnOffGlobalNotifications()
```php
$page->turnOffGlobalNotifications();
```
